﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoginConsola
{
    class Tutoria:Registro
    {
        private DateTime fecha;

        public DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }
        private string aula;

        public string Aula
        {
            get { return aula; }
            set { aula = value; }
        }
        private DateTime hora;

        public DateTime Hora
        {
            get { return hora; }
            set { hora = value; }
        }
        private string asignatura;

        public string Asignatura
        {
            get { return asignatura; }
            set { asignatura = value; }
        }
        private string tema;

        public string Tema
        {
            get { return tema; }
            set { tema = value; }
        }



    }
}

