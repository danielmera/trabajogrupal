﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoginConsola
{
    class RegistroLogin:ElegirTutoria
    {
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
        private string cedula;

        public string Cedula
        {
            get { return cedula; }
            set { cedula = value; }
        }
        private string usuario;

        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        private string crearContraseña;

        public string CrearContraseña
        {
            get { return crearContraseña; }
            set { crearContraseña = value; }
        }
        private string confirmarContraseña;

        public string ConfirmarContraseña
        {
            get { return confirmarContraseña; }
            set { confirmarContraseña = value; }
        }
        private string genero;

        public string Genero
        {
            get { return genero; }
            set { genero = value; }
        }

    }
}
